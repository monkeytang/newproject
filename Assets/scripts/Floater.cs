﻿// Floater v0.0.2
// by Donovan Keith
//
// [MIT License](https://opensource.org/licenses/MIT)

using UnityEngine;
using System.Collections;

// Makes objects float up & down while gently spinning.
public class Floater : MonoBehaviour
{
    // User Inputs
   // public Rigidbody redSkull;

    public float degreesPerSecond = 25.0f;
    public float amplitude = 0.5f;
    public float frequency = 0.1f;
    public float startPos = 1.6f;

    // Position Storage Variables
    Vector3 posOffset = new Vector3();
    Vector3 tempPos = new Vector3();

    // Use this for initialization
    void Start()
    {
        // Store the starting position & rotation of the object
        posOffset = transform.position;
        // sets the random transform position
        InvokeRepeating("SetRandomPos",1,2f);
    }

    // Update is called once per frame
    void Update()
    {
       
        // Spin object around Y-Axis
        transform.Rotate(new Vector3(Time.deltaTime * degreesPerSecond, Time.deltaTime * degreesPerSecond, Time.deltaTime * degreesPerSecond), Space.World);
     
    }

    void SetRandomPos()
    {
        Vector3 temp = new Vector3(Random.Range(startPos, 1.6f), Random.Range(-1.6f, 1.6f), Random.Range(-1.6f, 3.6f));
        this.transform.position = temp;
    }



}